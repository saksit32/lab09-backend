package se331.lab.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.lab.rest.entity.Lecturer;

public interface LecturerRepository extends CrudRepository<Lecturer, Long> {

}
