package se331.lab.rest.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;


@Data
@Builder
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    Integer penAmount;
    String description;
    @ManyToOne
    Lecturer advisor;
    @ManyToMany(mappedBy = "students")
    Set<Course> enrollCourses;
}