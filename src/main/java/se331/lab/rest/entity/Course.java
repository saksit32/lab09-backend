package se331.lab.rest.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Builder
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String content;
    String courseId;
    String courseName;
    @ManyToOne
    Lecturer lecture;
    @ManyToMany
    Set<Student> students;
}
