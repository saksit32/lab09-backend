package se331.lab.rest.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Builder
@Entity
public class Lecturer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    String surname;
    @OneToMany(mappedBy = "lecture")
    Set<Course> courses;
    @OneToMany(mappedBy = "advisor")
    Set<Student> Advisees;
}