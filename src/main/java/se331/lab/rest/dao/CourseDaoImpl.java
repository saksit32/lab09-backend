package se331.lab.rest.dao;

import se331.lab.rest.entity.Course;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

public class CourseDaoImpl implements CourseDao {

    public CourseDaoImpl() {
        this.courses = new ArrayList<>();
        this.courses.add(Course.builder()
                .id(1l)
                .courseId("953331")
                .courseName("Component Based Software Dev")
                .content("Nothing just for fun")
                .lecturer(Lecturer.builder()
                        .name("Jayakrit")
                        .surname("Hirisajja")
                        .build())
                .build());
        this.courses.add(Course.builder()
                .id(2l)
                .courseId("953xxx")
                .courseName("X project")
                .content("Do not know what to study")
                .lecturer(Lecturer.builder()
                        .name("Chartchai")
                        .surname("Doungsa-ard")
                        .build())
                .build());
    }

    @Override
    public List<Course> getCourses() {
        return null;
    }

    @Override
    public Course getCourse(Long id) {
        return null;
    }

    @Override
    public Course saveCourse(Course course) {
        return null;
    }
}
