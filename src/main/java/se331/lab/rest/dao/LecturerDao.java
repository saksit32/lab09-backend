package se331.lab.rest.dao;

import se331.lab.rest.entity.Lecturer;

import java.util.List;

public interface LecturerDao {
    List<Lecturer> getLecturers();
    Lecturer getLecturer(Long id);
    Lecturer saveLecturer(Lecturer lecturer);
}
